package com.example.demo.repo;

import com.amazonaws.regions.Regions;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDB;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClientBuilder;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBMapper;
import com.example.demo.domain.UserDO;
import org.springframework.stereotype.Component;

import java.util.HashSet;

@Component
public class AwsConnection {
    public AmazonDynamoDB newAwsConnection(){
        AmazonDynamoDBClientBuilder builder = AmazonDynamoDBClientBuilder.standard();
        AmazonDynamoDB ddb = builder.withRegion(Regions.AP_SOUTH_1).build();
        return ddb;
    }

    public DynamoDBMapper getNewMapper(){
        AwsConnection awsConnection = new AwsConnection();
        AmazonDynamoDB client = awsConnection.newAwsConnection();
        DynamoDBMapper mapper = new DynamoDBMapper(client);
        return mapper;
    }

    public Integer generateRandomNumberInRange(Integer min, Integer max){
        return (int) (Math.random()*(max - min + 1)+min);
    }

    private AmazonDynamoDB client = newAwsConnection();
    private DynamoDBMapper mapper = new DynamoDBMapper(client);

    public void generateRandomDataForUser(){
        AmazonDynamoDB client = newAwsConnection();
        DynamoDBMapper mapper = new DynamoDBMapper(client);
        
        for (Integer i =0; i<5; i++){
            System.out.printf("inserting data number, %s \n", i.toString());
            UserDO user = new UserDO();
            user.setAge(generateRandomNumberInRange(10, 70));
            user.setEggScore(Math.random()*(100));
            user.setLongitude(Math.random()*100);
            user.setAltitude(Math.random()*100);
            user.setNonVegScore(Math.random()*100);
            user.setName(i.toString());
            user.setVegScore(Math.random()*100);
            user.setPriceHighScore(Math.random()*100);
            user.setPriceLowScore(Math.random()*100);
            user.setPriceMediumScore(Math.random()*100);
            user.setSpicyScore(Math.random()*100);
            user.setSweetScore(Math.random()*100);
            user.setFollowers(new HashSet<String>(){{add("dummy");}});
            user.setFollowing(new HashSet<String>(){{add("dummy");}});
            user.setPostIds(new HashSet<String>(){{add("dummy");}});
            mapper.save(user);
        }
    }

    public UserDO getUser(String userId){
        UserDO user = mapper.load(UserDO.class, userId);
        return user;
    }

    public void updateUser(UserDO user){
        mapper.save(user);
    }
}
