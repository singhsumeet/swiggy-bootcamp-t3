package com.example.demo.repo;

import com.amazonaws.services.dynamodbv2.AmazonDynamoDB;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBMapper;
import com.example.demo.controller.PostsController;
import com.example.demo.domain.PostsDO;
import com.example.demo.domain.UserDO;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Set;

@Repository

public class PostsRepo {
    private AwsConnection awsConnection = new AwsConnection();
    private DynamoDBMapper mapper = awsConnection.getNewMapper();

    public PostsDO getPostFromPostId(String postId){
        PostsDO post = mapper.load(PostsDO.class, postId);
        return post;
    }

    public PostsDO createNewPost(PostsController.NewPostRequest newPostRequest){
        PostsDO post = new PostsDO();
        post.setLikes(0);
        post.setText(newPostRequest.text);
        post.setTimestamp(LocalDateTime.now());
        post.setUserId(newPostRequest.userId);
        UserDO user = awsConnection.getUser(newPostRequest.userId);
        post.setName(user.getName());
        Set<String> postIds = user.getPostIds();
        mapper.save(post);
        postIds.add(post.getPostId());
        user.setPostIds(postIds);
        mapper.save(user);
        return post;
    }
}
