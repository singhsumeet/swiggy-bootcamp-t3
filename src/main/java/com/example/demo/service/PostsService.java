package com.example.demo.service;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBMapper;
import com.example.demo.controller.PostsController;
import com.example.demo.domain.PostsDO;
import com.example.demo.domain.UserDO;
import com.example.demo.repo.AwsConnection;
import com.example.demo.repo.PostsRepo;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Set;

public class PostsService {
    private AwsConnection awsConnection = new AwsConnection();
    private PostsRepo postsRepo = new PostsRepo();

    public List<PostsDO> getPostsForUser(String userId){
        UserDO user = awsConnection.getUser(userId);
        List<PostsDO> postList = new ArrayList<>();

        for (String followingId : user.getFollowing()){
            UserDO friend = awsConnection.getUser(followingId);
            if (friend != null) {
                Set<String> postIds = friend.getPostIds();
                for (String pid : postIds){
                    PostsDO post = postsRepo.getPostFromPostId(pid);
                    if (post != null)
                        postList.add(post);
                }
            }
        }
        Set<String> postIds = user.getPostIds();
        for (String pid : postIds){
            PostsDO post = postsRepo.getPostFromPostId(pid);
            if (post != null)
                postList.add(post);
        }


        Collections.sort(postList, Comparator.comparing(PostsDO::getTimestamp));

        return postList;
    }

    public PostsDO createNewPost(PostsController.NewPostRequest newPostRequest){
        // check for post legitimacy
        return postsRepo.createNewPost(newPostRequest);
    }
}
