package com.example.demo.service;

import com.example.demo.controller.PostsController;
import com.example.demo.controller.UsersController;
import com.example.demo.domain.PostsDO;
import com.example.demo.domain.UserDO;
import com.example.demo.repo.AwsConnection;

import java.util.ArrayList;
import java.util.Set;

public class UsersService {
    private AwsConnection awsConnection = new AwsConnection();

    public String followUsers(String userId, UsersController.FollowRequestBody body){
        ArrayList<String> userIdsToFollow = body.userIdsToFollow;
        UserDO user = awsConnection.getUser(userId);
        Set<String> initialFollowing = user.getFollowing();

        for(String id : userIdsToFollow){
            initialFollowing.add(id);
            UserDO friend = awsConnection.getUser(id);
            Set<String> followers = friend.getFollowers();
            followers.add(userId);
            friend.setFollowers(followers);
            awsConnection.updateUser(friend);
        }

        user.setFollowing(initialFollowing);
        awsConnection.updateUser(user);
        return "OK";
    }

    public String unFollowUsers(String userId, UsersController.UnFollowRequestBody body){
        ArrayList<String> userIdsToUnfollow = body.userIdsToUnFollow;
        UserDO user = awsConnection.getUser(userId);
        Set<String> initialFollowing = user.getFollowing();

        for(String id : userIdsToUnfollow){
            initialFollowing.remove(id);
            UserDO friend = awsConnection.getUser(id);
            Set<String> followers = friend.getFollowers();
            followers.remove(userId);
            friend.setFollowers(followers);
            awsConnection.updateUser(friend);
        }

        user.setFollowing(initialFollowing);
        awsConnection.updateUser(user);
        return "following updated successfully";
    }
}
