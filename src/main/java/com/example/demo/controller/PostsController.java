package com.example.demo.controller;

import com.amazonaws.services.dynamodbv2.xspec.S;
import com.example.demo.domain.PostsDO;
import com.example.demo.repo.PostsRepo;
import com.example.demo.service.PostsService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RequestMapping("posts")
@RestController
public class PostsController {
    private PostsRepo postsRepo = new PostsRepo();
    private PostsService postsService = new PostsService();

    @GetMapping("/{postId}")
    public PostsDO getPostWithPostId(@PathVariable String postId){
        return postsRepo.getPostFromPostId(postId);
    }

    @GetMapping("/user/{userId}/posts")
    public List<PostsDO> getUserPosts(@PathVariable String userId) {
        return postsService.getPostsForUser(userId);
    }

    @PostMapping("/new")
    public PostsDO newPost(@RequestBody NewPostRequest newPostRequest) {
        return postsService.createNewPost(newPostRequest);
    }

    public static class NewPostRequest{
        public String userId;
        public String text;
    }
}
