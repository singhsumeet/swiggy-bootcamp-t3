package com.example.demo.controller;

import com.example.demo.domain.UserDO;
import com.example.demo.repo.AwsConnection;
import com.example.demo.service.UsersService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;

@RestController
@RequestMapping("/users")
public class UsersController {
    private AwsConnection awsConnection = new AwsConnection();
    private UsersService userService = new UsersService();

    @GetMapping("{id}")
    public UserDO getUser(@PathVariable String id){
        return awsConnection.getUser(id);
    }

    @PostMapping("{id}/follow")
    public String followUsers(@RequestBody FollowRequestBody body, @PathVariable String id){
        return userService.followUsers(id, body);
    }

    @PostMapping("{id}/unfollow")
    public String unfollowUsers(@RequestBody UnFollowRequestBody body, @PathVariable String id){
        return userService.unFollowUsers(id, body);
    }

    @GetMapping("/")
    public String getHello(){
        return "Hello";
    }

    public static class FollowRequestBody {
        public ArrayList<String> userIdsToFollow;
    }

    public static class UnFollowRequestBody {
        public ArrayList<String> userIdsToUnFollow;
    }

}